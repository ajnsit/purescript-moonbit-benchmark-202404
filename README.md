# PureScript, Moonbit, KotlinJS, and Plain Javascript benchmarks.

## About the PureScript benchmarks

The PureScript benchmark uses [PureScript Backend Optimizer](https://github.com/aristanetworks/purescript-backend-optimizer), which provides an optimising ecmascript backend.

The PureScript benchmark code does not use the in built Array library but instead rewrites everything in 100% PureScript code because it can be optimised by the backend. The base Array lib that comes with PureScript on the other hand, uses native JS FFI.

## Original REPO

[The original Moonbit benchmark repo](https://github.com/moonbit-community/benchmark-202404.git)
## Original Instructions follow

The benchmark code for `iter` is in the bench_iter subfolder.

The benchmark code for `json5` is in the bench_json5 subfolder.

## How to run the benchmark

Assuming you already have toolchains of nodejs, purescript, moonbit and kotlin.

iter

```
cd bench_iter
pnpm install
pnpm build
pnpm start
```

json5

```
cd bench_json5
pnpm install
pnpm build
pnpm start
```

The code repository already has pre-compiled js uploaded, so you can skip `pnpm build` and directly proceed with the benchmarking.

## Benchmark Results

iter

```
┌─────────┬──────────────┬─────────────┬────────────────────┬──────────┬─────────┐
│ (index) │ Task Name    │ ops/sec     │ Average Time (ns)  │ Margin   │ Samples │
├─────────┼──────────────┼─────────────┼────────────────────┼──────────┼─────────┤
│ 0       │ 'Moonbit'    │ '34,67,542' │ 288.38869989829305 │ '±0.06%' │ 1733772 │
│ 1       │ 'Plain Js'   │ '74,816'    │ 13365.983827421464 │ '±0.54%' │ 37409   │
│ 2       │ 'Kotlin Js'  │ '1,90,241'  │ 5256.474017304151  │ '±0.38%' │ 95121   │
│ 3       │ 'PureScript' │ '4,99,456'  │ 2002.1768597161156 │ '±0.70%' │ 249729  │
└─────────┴──────────────┴─────────────┴────────────────────┴──────────┴─────────┘
```

json5

The PureScript benchmark for JSON parsing is not written yet.

```
┌─────────┬────────────────────────┬──────────┬────────────────────┬──────────┬─────────┐
│ (index) │ Task Name              │ ops/sec  │ Average Time (ns)  │ Margin   │ Samples │
├─────────┼────────────────────────┼──────────┼────────────────────┼──────────┼─────────┤
│ 0       │ 'MoonbitJson5.parse'   │ '24,099' │ 41493.79917012389  │ '±0.57%' │ 12050   │
│ 1       │ 'json5.parse'          │ '2,825'  │ 353944.00707713875 │ '±0.79%' │ 1413    │
└─────────┴────────────────────────┴──────────┴────────────────────┴──────────┴─────────┘
```

Tested on node v21.7.3, Thinkpad P14s AMD Gen 4, AMD Ryzen™ 7 PRO 7840U

