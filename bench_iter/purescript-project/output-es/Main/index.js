const run = x => {
  const loop = loop$a0$copy => loop$a1$copy => {
    let loop$a0 = loop$a0$copy, loop$a1 = loop$a1$copy, loop$c = true, loop$r;
    while (loop$c) {
      const n = loop$a0, acc = loop$a1;
      if (n < 0) {
        loop$c = false;
        loop$r = acc;
        continue;
      }
      loop$a0 = n - 1 | 0;
      loop$a1 = (() => {
        const $0 = x[n].members;
        const loop$1 = loop$1$a0$copy => loop$1$a1$copy => {
          let loop$1$a0 = loop$1$a0$copy, loop$1$a1 = loop$1$a1$copy, loop$1$c = true, loop$1$r;
          while (loop$1$c) {
            const n$1 = loop$1$a0, acc$1 = loop$1$a1;
            if (n$1 < 0) {
              loop$1$c = false;
              loop$1$r = acc$1;
              continue;
            }
            loop$1$a0 = n$1 - 1 | 0;
            loop$1$a1 = (() => {
              const $1 = $0[n$1];
              if ($1.gender) {
                const $2 = $1.score + 5 | 0;
                if (100 > $2 ? $2 >= 90 : true) { return acc$1 + 1 | 0; }
              }
              return acc$1;
            })();
          }
          return loop$1$r;
        };
        return loop$1($0.length - 1 | 0)(acc);
      })();
    }
    return loop$r;
  };
  return loop(x.length - 1 | 0)(0);
};
export {run};
