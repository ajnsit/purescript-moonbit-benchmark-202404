module Main (run) where

import Data.Boolean (otherwise)
import Data.Eq ((==))
import Data.Function ((<<<), (>>>))
import Data.Ord ((>), (>=))
import Data.Semiring ((+))
import Fold (filterF, flatMapF, foldF, fromArray, mapF)

type Data = { members :: Array Person }
type Person = { gender :: Boolean, score :: Int }

run :: Array Data -> Int
run =
  fromArray
    >>> flatMapF (fromArray <<< _.members)
    >>> filterF _.gender
    >>> mapF (\x -> min 100 (x.score + 5))
    >>> mapF grade
    >>> filterF (_ == 'A')
    >>> foldF (\_ x -> x+1) 0

min :: Int -> Int -> Int
min x y
  | x > y = y
  | otherwise = x

grade :: Int -> Char
grade score
  | score >= 90 = 'A'
  | score >= 80 = 'B'
  | score >= 70 = 'C'
  | score >= 60 = 'D'
  | otherwise = 'F'

