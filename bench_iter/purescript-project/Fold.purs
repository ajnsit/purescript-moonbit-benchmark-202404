-- @inline export fromArray arity=1
module Fold where

import Data.Array as Array
import Data.Boolean (otherwise)
import Data.Function (($), (<<<))
import Data.Maybe (Maybe(..))
import Data.Ord ((<))
import Data.Ring ((-))
import Partial.Unsafe (unsafePartial)

newtype Fold a = Fold (forall r. (a -> r -> r) -> r -> r)

unwrapF :: forall a b. Fold a -> (a -> b -> b) -> b -> b
unwrapF (Fold next) = next

flatMapF :: forall a b. (a -> Fold b) -> Fold a -> Fold b
flatMapF f (Fold next) = Fold \cons nil -> 
  next (\a -> unwrapF (f a) cons) nil

foldF :: forall a b. (a -> b -> b) -> b -> Fold a -> b
foldF f b (Fold next) = next f b

mapF :: forall a b. (a -> b) -> Fold a -> Fold b
mapF f (Fold next) = Fold \cons nil -> next (cons <<< f) nil

filterMapF :: forall a b. (a -> Maybe b) -> Fold a -> Fold b
filterMapF f (Fold next) = Fold \cons nil ->
  next
    ( \a as ->
        case f a of
          Just b ->
            cons b as
          Nothing ->
            as
    )
    nil

filterF :: forall a. (a -> Boolean) -> Fold a -> Fold a
filterF p = filterMapF (\a -> if p a then Just a else Nothing)

fromArray :: forall a. Array a -> Fold a
fromArray arr = Fold \cons nil -> do
  let
    loop n acc
      | n < 0 =
          acc
      | otherwise =
          loop (n - 1) $ cons (unsafePartial (Array.unsafeIndex arr n)) acc
  loop (Array.length arr - 1) nil

